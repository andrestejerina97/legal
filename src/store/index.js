import {createStore} from 'vuex';
const urlApiUser = 'https://app.isohub.org/public/api/auth/me';

const store = createStore({
    state(){
        return{
            username:null,
            auth:false,
            remember:false,
            name:null
        }
    },
    mutations:{
        doLogin(state,username){
            state.auth=true;
            state.username=username;
        },
        doLogout(state){
            state.auth=false;
            state.username=null;
        },
        doRemember(state){
            state.remember=true;
        }
    },
    getters:{
        isLogin(state) {
            /*Este código hace los mismo que el if, según Jorge. Aporte de Jorge.*/
            /* return sessionStorage.getItem('token') !== null ||
                   sessionStorage.getItem('token') !== ""; */
            /* if(sessionStorage.getItem('token')){
                return true
            }else{
                return false
            } */
            return state.auth
        },
        isRemember(state){
            return state.remember
        }
    },
    actions:{
        doLogin(context,username){
            context.commit('doLogin',username)
        },
        doRemember(context){
            context.commit('doRemember')
        }
    }
})

export default store;