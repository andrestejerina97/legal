import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import router from './router'
import VueCookies from 'vue3-cookies'
import store from './store'
const app = createApp(App);

app.use(router);
app.use(VueCookies, {
    expireTimes: "3000d",
    path: "/",
    domain: "",
    secure: false,
    sameSite: "None"
});
app.use(store);
app.mount('#app');