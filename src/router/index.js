import { createWebHistory, createRouter } from "vue-router";
import Home from "/src/views/Home.vue";
import Dashboard from "/src/views/Dashboard.vue";
import store from "/src/store/index.js";
import UserDashboard from "/src/views/UserDashboard.vue";
const routes = [
  {
    path: "/",
    name: "Home",
    component:Home
  },
  {
    path:"/dashboard/user/:id",
    name:"Dashboard",
    component:Dashboard,
    meta:{requiresAuth: true}
  },
  {
    path:"/user-dashboard/user/:id",
    name:"UserDashboard",
    component:UserDashboard,
    meta:{requiresAuth: true}
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to,from,next)=>{
  if(to.meta.requiresAuth){
    if(store.getters.isLogin || store.getters.isRemember){
      next();
    }else{
      next({name:"Home"});
    }
  }else{
    next();
  }
});

export default router;